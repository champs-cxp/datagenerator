drop program dgen_updt_dx:dba go
create program dgen_updt_dx:dba
 
prompt
    "Output to File/Printer/MINE: (MINE)" = "MINE"             ;* Enter or select the printer or file name to send this report to.
    , "Lookback: (1) " =  1                  ;* Please enter the Diagnosis Display
;    , "Enter the Diagnosis Source Identifier from Bedrock: " = ""   ;* Enter the Diagnosis Source Identifier from IFG/CMT_Tool
 
with OUTDEV,PROMPT_LOOKBACK ; DiagnosisDisplay, IFGIdentifier
 
 
/**************************************************************
; DVDev DECLARED SUBROUTINES
**************************************************************/
 
/**************************************************************
; DVDev DECLARED VARIABLES
**************************************************************/
declare cntr = i4 with protect,noconstant(0)
declare x = i4 with protect,noconstant(0)
declare C_P_LOOKBACK = i4 with protect, constant($PROMPT_LOOKBACK)
declare C_DGEN_CONTRIBUTOR_SYSTEM_CD = f8 with public, constant(uar_get_code_by("DISPLAY", 89, "DGEN"))
declare C_MEDICAL_CLASSIFICATION_CD = f8 with public, constant(uar_get_code_by("MEANING", 12033, "MEDICAL"))
declare C_CONFIRMED_CONFIRMATION_STATUS_CD = f8 with public, constant(uar_get_code_by("MEANING", 12031, "CONFIRMED"))

/**************************************************************
; DVDev Start Coding
**************************************************************/

free record  updtdx
record updtdx (
    1 updtdx_cnt = i4
    1 qual [*]
       2 diag_id = f8 ;place holder for diagnosis_id from nomenclature table
       2 nom_source_string = vc
    )
     
select into $OUTDEV
from diagnosis d
    ,nomenclature n
;   ,person p
plan d where d.contributor_system_cd = C_DGEN_CONTRIBUTOR_SYSTEM_CD  ;   DGEN
         and d.updt_dt_tm > sysdate - C_P_LOOKBACK
         and d.active_ind = 1 
         and d.diagnosis_display is null /* Only touch what was not previously updated */
join n where n.nomenclature_id = d.nomenclature_id
    ;and n.source_identifier = $IFGIdentifier
;join p where p.person_id = d.person_id
;   and p.person_id =       ;OPTION TO QUALIFY ON PERSON_ID FOR TROUBLESHOOTING
  
;Begin Report for updating Dx Table
head report
    cntr = 0,
    stat = alterlist (updtdx -> qual, 10)
  
detail

    cntr = (cntr +1)
  
    if ((mod(cntr,10)=1) and (cntr!=1)) 
         stat = alterlist(updtdx -> qual, (cntr+9))
    endif
   
    updtdx -> qual [cntr] -> diag_id = d.diagnosis_id
    updtdx -> qual [cntr] -> nom_source_string = n.source_string
 
foot report
    stat = alterlist (updtdx -> qual, cntr)
    updtdx -> updtdx_cnt = cntr
    with nocounter
 
for (x = 1 to value(updtdx -> updtdx_cnt))

   call echo( updtdx -> qual[x] -> diag_id) 
   call echo( updtdx -> qual[x] -> nom_source_string) 

   ;Update Dx Table
   update into diagnosis d
      set  d.diagnosis_group = updtdx -> qual[x] -> diag_id ; diagnosis group will be set to diagnosis_id
         , d.classification_cd = C_MEDICAL_CLASSIFICATION_CD ;code value for Medical
         , d.confirmation_status_cd =  C_CONFIRMED_CONFIRMATION_STATUS_CD ; code value for Confirmed
         , d.diagnosis_display =  updtdx -> qual[x] -> nom_source_string
    where (d.diagnosis_id = updtdx -> qual[x] -> diag_id)
 
    if (curqual > 0)  
       /**** DEBUG 
       select into $OUTDEV
              p.name_last_key
             ,p.name_first
             ,p.person_id
             ,d.diagnosis_group
             ,d.end_effective_dt_tm
             ,d.classification_cd
             ,Classification = uar_get_code_display(d.classification_cd)
             ,d.confirmation_status_cd
             ,Confirm_status = uar_get_code_display(d.confirmation_status_cd)
             ,d.diagnosis_display
             ,Contributor_system= uar_get_code_display(d.contributor_system_cd)
             ,d.updt_dt_tm
             ,d.updt_id
        from diagnosis d
            ,nomenclature n
            ,person p
        plan p
        join d where d.person_id = p.person_id
                 and d.diagnosis_id = updtdx -> qual[x] -> diag_id
        join n where n.nomenclature_id = d.nomenclature_id
        *** DEBUG END */
    commit
else
   rollback
endif
endfor
;
;rollback
;commit
 
/**************************************************************
; DVDev DEFINED SUBROUTINES
**************************************************************/
 
end
go
