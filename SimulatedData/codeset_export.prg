/* Codeset aliases for export to Content Manager 
 * The columns must follow what is specified in the Content Manager template 
 * (Go to the menu, File > Generate .CSV Templates and specify CVAlias as the domain)
 *
 * The contents can be imported into the CSV template (or the headings copied from the template into the csv) 
 * Highlighting the output of DVD and Ctrl-v to copy, Ctrl-V to paste into the CSV templaet works, too,a although
 * not sure of clipboard limitations. 
 */
select cv.code_set
     , CODE_VALUE_DISPLAY=cv.display
     , CONTRIBUTOR_SOURCE=cvcs.display
     , cva.alias
     , cva.alias_type_meaning
     , cva.primary_ind
     , direction="Inbound" /* 1 - inbond, 2 - outbound */
     , alias_active_indicator=1
     , delete_alias_indicator=0
     , cv.cdf_meaning
     , cv.active_ind
from code_value_alias cva
   , (inner join code_value cv
             on cv.code_value = cva.code_value
            and cv.active_ind = 1)
   , (inner join code_value cvcs
             on cvcs.code_value = cva.contributor_source_cd
            and cvcs.code_set = 73
            and cvcs.display_key = 'DGEN') ;replace with display value for contributor source
   , (inner join code_value_set cvs
              on cvs.code_set = cv.code_set)
where cva.code_set != 220 /* exclude location aliases -- requires a different template */
 /* debug:   and cva.code_set = 8 */
order by cv.CODE_VALUE,cvcs.display,cv.code_set, cvs.display, cv.display,cva.alias
go
