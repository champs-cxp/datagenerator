/* Get location aliases for EPIC_ADT and the Data Generator */
select distinct
     CONTRIBUTOR_SOURCE=cv.display_key
    ,FACILITY = uar_get_code_display(lgf.parent_loc_cd)
    ,BUILDING = uar_get_code_display(lgf.child_loc_cd)
    ,UNIT = uar_get_code_display(lgb.child_loc_cd)
    ,ROOM = uar_get_code_display(lgu.child_loc_cd)
    ,BED = uar_get_code_display(lgr.child_loc_cd)
    ,FAC_ALIAS = cva.alias
    ,BLD_ALIAS = SUBSTRING(findstring("^~",cvaf.alias,1,1)+2,40,cvaf.alias)
    ,UNIT_ALIAS = SUBSTRING(findstring("^~",cvab.alias,1,1)+2,40,cvab.alias)
    ,ROOM_ALIAS = SUBSTRING(findstring("^~",cvau.alias,1,1)+2,40,cvau.alias)
    ,BED_ALIAS = SUBSTRING(findstring("^~",cvar.alias,1,1)+2,40,cvar.alias)
from code_value cv
 , (inner join code_value_alias cva
            on cva.code_set = 220
           and cva.contributor_source_cd = cv.code_value)
 , (inner join location l
            on l.location_cd = cva.code_value
           AND l.active_ind = 1)
 , (inner join location_group lgf
            on lgf.parent_loc_cd = l.location_cd
           and lgf.location_group_type_cd = 783.00) ;code_value for Facility on CS 222
 , (inner join code_value_alias cvaf
            on cvaf.code_value = lgf.child_loc_cd
           and cvaf.code_set = 220
           and cvaf.contributor_source_cd = cv.code_value)
 , (inner join location_group lgb
            on lgb.root_loc_cd = 0
           and lgb.parent_loc_cd = lgf.child_loc_cd)
 , (inner join code_value_alias cvab
            on cvab.code_value = lgb.child_loc_cd
           and cvab.code_set = 220
           and cvab.contributor_source_cd = cv.code_value)
 , (left  join location_group lgu
            on lgu.root_loc_cd = 0
           and lgu.parent_loc_cd = lgb.child_loc_cd)
 , (inner join code_value_alias cvau
            on cvau.code_value = lgu.child_loc_cd
           and cvau.code_set = 220
           and cvau.contributor_source_cd = cv.code_value)
 , (left  join location_group lgr
            on lgr.root_loc_cd = 0
           and lgr.parent_loc_cd = lgu.child_loc_cd)
 , (inner join code_value_alias cvar
            on cvar.code_value = lgr.child_loc_cd
           and cvar.code_set = 220
           and cvar.contributor_source_cd = cv.code_value)
where cv.active_ind = 1
  and cv.code_set = 73 
  and cv.display_key in ('DGEN','EPICADT') ;replace with display value for contributor source
order by
     CONTRIBUTOR_SOURCE
    ,FACILITY
    ,BUILDING
    ,UNIT
    ,ROOM
    ,BED
go
