/* Emergency contact tables filled up by NK1 segment */
select p.name_full_formatted
     , relation=uar_get_code_display(epr.person_reltn_cd)
     , relation_type=uar_get_code_display(epr.person_reltn_type_cd)
     , epr_contributor_system=uar_get_code_display(epr.contributor_system_cd)
     , ad.*
from encntr_person_reltn epr
, (inner join address ad
           on ad.parent_entity_id = epr.related_person_id
          and ad.parent_entity_name = 'PERSON' )
, (inner join person p
           on p.person_id = ad.parent_entity_id)
where epr.encntr_id =  91958634
go
