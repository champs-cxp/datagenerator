#!/bin/bash

#Number of lines to process at a time
N=$2

# This location can be hard-coded since this is configured at the comm server
adt_file="$cust_reports/dgen/adt.in"

# Read the first N lines from the file and put in array lineArray
mapfile -t -n $N lineArray < $1

# string separator
IFS=','

# The HL7 text file must be separated by carriage returns, not line feeds
# Line feeds will crash the comm server
CR=$'\r'

# These are HL7 parameters that is computed by the data generator
ADMIT_TYPE="1"         # 1 = Emergency ,  2= Urgent ,3 = Elective ( Outpatient and Day Surgery)
                       # Inpatient and Obs come from ED, so use 1. No correlation with encounter type
ASSIGN_AUTH="CHB"      #This is set at the ESI config, but should be computed by the data generator
ENCOUNTER_STATUS="D"   # Since A03, D= dischargea.  this cannot be set in the csv file, since the encounter are still active

# Process the N lines
errcd=0
for line in "${lineArray[@]}"; do

     # Read the comman separated string
     read -r -a strarr <<< "$line"

     # Assign elements of the array
     MRN="${strarr[0]}"
     CSN="${strarr[1]}"
     LASTNAME="${strarr[2]}"
     FIRSTNAME="${strarr[3]}"
     DOB="${strarr[4]}"
     GENDER="${strarr[5]}"
     REG_DT_TM=`echo ${strarr[6]}${strarr[7]} | sed 's/ //g'`
     DISCH_DT_TM=`echo ${strarr[8]}${strarr[9]} | sed 's/ //g'`
     ENCOUNTER_TYPE="${strarr[10]}"
     FACILITY="${strarr[11]}"
     BUILDING="${strarr[12]}"
     NURSE_UNIT="${strarr[13]}"
     ROOM="${strarr[14]}"
     BED="${strarr[15]}"

    
     #Minimal variables to set
     MSH7=$DISCH_DT_TM
     MSH10=$MRN

     EVN2=${DISCH_DT_TM}

     PID2=$MRN
     PID3_1=$MRN
     PID3_4=${FACILITY}
     PID3_1_2=$MRN
     PID5_1=${LASTNAME}
     PID5_2=${FIRSTNAME}
     PID7=${DOB}
     PID8=${GENDER}
     PID18_1=${CSN}
     PID18_4=${ASSIGN_AUTH}

     PV13_1=${NURSE_UNIT}
     PV13_2=${ROOM}
     PV13_3=${BED}
     PV13_4=${FACILITY}
     PV13_7=${BUILDING}
     PV14=${ADMIT_TYPE}
     PV118=${ENCOUNTER_TYPE}
     PV119_1=${CSN}
     PV119_4=${ASSIGN_AUTH}
     PV139=${FACILITY}
     PV141=${ENCOUNTER_STATUS}
     PV144=${REG_DT_TM}
     PV145=${DISCH_DT_TM}


     # Construct the A03
     MSH="MSH|^~\&|DGEN|CHB|HNAM|CHB|${MSH7}||ADT^A03|${MSH10}||2.2${CR}"
     EVN="EVN|A03|${EVN2}${CR}"
     PID="PID|1|${PID2}|${PID3_1}^^^${PID3_4}~${PID3_1_2}||${PID5_1}^${PID5_2}^^||${PID7}|${PID8}|||^^^^^^^^|||||||${PID18_1}^^^${PID18_4}|||||||${CR}"
     PV1="PV1|||${PV13_1}^${PV13_2}^${PV13_3}^${PV13_4}^^^${PV13_7}|${PV14}|||^^|^^|||||||||^^|${PV118}|${PV119_1}^^^${PV119_4}||||||||||||||||||||${PV139}||${PV141}|||${PV144}|${PV145}"

     echo "${MSH}${EVN}${PID}${PV1}" > $adt_file
     #echo "MSH|^~\&|DGEN|CHB|HNAM|CHB|${MSH7}||ADT^A03|${MSH10}||2.2${CR}EVN|A03|${EVN2}${CR}PID|1|${PID2}|${PID3_1}^^^${PID3_4}~${PID3_1_2}||${PID5_1}^${PID5_2}^^||${PID7}|${PID8}|||^^^^^^^^|||||||${PID18_1}^^^${PID18_4}|||||||${CR}PV1|||${PV13_1}^${PV13_2}^${PV13_3}^${PV13_4}^^^${PV13_7}|${PV14}|||^^|^^|||||||||^^|${PV118}|${PV119_1}^^^${PV119_4}||||||||||||||||||||${PV139}||${PV141}|||${PV144}|${PV145}" >> $adt_file
     sleep 30
done
errcd=$?

echo "${#lineArray[@]} A03  messages created.  Error code: $errcd"

# Delete the N lines
if [[ $errcd -eq "0" ]]; then
    sed -i -e "1,${N}d" $1
    echo "${N} rows deleted from $1"
fi
