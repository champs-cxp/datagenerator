select oc.primary_mnemonic
 ;    , ANCILLARY_SYNOYNYM=ocs.mnemonic
     , Activity_type=cvact.display
     , DTA=cvdta.display
     , EVENT=cvec.display
     , o.order_id
     , ce.result_val
     , UNIT=uar_get_code_display(ce.result_units_cd)
     , ce.normal_low
     , ce.normal_high
     , ce.*    
from orders o
, (inner join order_catalog oc
           on oc.catalog_cd = o.catalog_cd)
;, (inner join order_catalog_synonym ocs
;          on ocs.catalog_cd = oc.catalog_cd
;         and ocs.mnemonic_type_cd = (select code_value
;                                       from code_value
;                                      where code_set = 6011
;                                        and cdf_meaning in ('ANCILLARY'))) ;we already have the primary
, (inner join service_directory sd
           on sd.catalog_cd = oc.catalog_cd)
, (inner join profile_task_r ptr
           on ptr.catalog_cd = sd.catalog_cd
          and ptr.active_ind = 1)
, (inner join discrete_task_assay dta 
           on dta.task_assay_cd = ptr.task_assay_cd
          and dta.active_ind = 1
          and dta.end_effective_dt_tm > sysdate)    
, (inner join code_value cvdta
           on cvdta.code_value = dta.task_assay_cd
          and cvdta.code_set = 14003
          and cvdta.active_ind = 1) 
, (inner join code_value cvact
           on cvact.code_value = dta.activity_type_cd
          and cvact.code_set = 106
          and cvact.active_ind = 1)
, (inner join code_value_event_r cvr
           on cvr.parent_cd = dta.task_assay_cd)
, (inner join code_value cvec
           on cvec.code_value = cvr.parent_cd)
, (inner join clinical_event ce
           on ce.order_id = o.order_id
          and ce.task_assay_cd = dta.task_assay_cd
          and ce.valid_until_dt_tm > sysdate)
where oc.primary_mnemonic = 'Complete Blood Count'
  and o.updt_dt_tm > sysdate - 1
order by o.order_id
       , oc.primary_mnemonic
 ;      , ocs.mnemonic
       , DTA
go