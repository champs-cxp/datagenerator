/* Get the DTA's associated with a lab orderable,
   so we get the DTA's to alias for an orderable.
   The ancilliary orderables are also listed so 
   we do not have to bother aliasing in codeset 200.
   Also included the event_cd so ther is a fallback
   if we are not matching orders  */
select oc.primary_mnemonic
     , ANCILLARY_SYNOYNYM=ocs.mnemonic
     , Activity_type=cvact.display
     , DTA=cvdta.display
     , EVENT=cvec.display
from order_catalog oc     
, (inner join order_catalog_synonym ocs
          on ocs.catalog_cd = oc.catalog_cd
         and ocs.mnemonic_type_cd = (select code_value
                                       from code_value
                                      where code_set = 6011
                                        and cdf_meaning in ('ANCILLARY'))) ;we already have the primary
, (inner join service_directory sd
           on sd.catalog_cd = oc.catalog_cd)
, (inner join profile_task_r ptr
           on ptr.catalog_cd = sd.catalog_cd
          and ptr.active_ind = 1)
, (inner join discrete_task_assay dta 
           on dta.task_assay_cd = ptr.task_assay_cd
          and dta.active_ind = 1
          and dta.end_effective_dt_tm > sysdate)    
, (inner join code_value cvdta
           on cvdta.code_value = dta.task_assay_cd
          and cvdta.code_set = 14003
          and cvdta.active_ind = 1) 
, (inner join code_value cvact
           on cvact.code_value = dta.activity_type_cd
          and cvact.code_set = 106
          and cvact.active_ind = 1)
, (inner join code_value_event_r cvr
           on cvr.parent_cd = dta.task_assay_cd)
, (inner join code_value cvec
           on cvec.code_value = cvr.parent_cd)
where oc.catalog_cd in (select code_value 
                          from code_value_alias 
                         where code_set = 200 
                           and contributor_source_cd in (select code_value 
                                                           from code_value 
                                                          where code_set = 73 
                                                            and display_key = 'DGEN')) 
;where oc.primary_mnemonic = 'Complete Blood*'
order by DTA,oc.primary_mnemonic,ocs.mnemonic,DTA
go
