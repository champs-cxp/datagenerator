select o.order_id     
     , oc.catalog_cd
     , oc.description
     , oc.primary_mnemonic
     , oc.dept_display_name
     , oef.oe_format_name
     , oeff.label_text
     , oeff.group_seq
     , oeff.field_seq
     , oeff.clin_line_ind
     , ACCEPT_FLAG=evaluate(oeff.accept_flag,
                                 0,"Required",
                                 1,"Optional",
                                 2,"No Display",
                                 3,"Display Only",
                                   "Unknown") /*   0 - required, 1 - optional, 2 - no display, 3 - display only */
     , oefi.description
     , oefi.field_type_flag
     , oefi.codeset
     , oefi.event_cd
     , oefi.oe_field_id
     , cva.alias
     , Contributor_Source=uar_get_code_display(cva.contributor_source_cd)
     , od.oe_field_value
     , od.oe_field_meaning
     , od.*
from orders o
, (inner join order_catalog oc
           on oc.catalog_cd = o.catalog_cd)
/* OEF template */
, (inner join order_entry_format oef
           on oef.oe_format_id = oc.oe_format_id
          and oef.action_type_cd in (select code_value from code_value where code_set = 6003 and cdf_meaning = 'ORDER' ))
/* Order customization */
, (inner join oe_format_fields oeff
           on oeff.oe_format_id = oef.oe_format_id
          and oeff.action_type_cd = oef.action_type_cd
          and oeff.accept_flag IN (0,1,2,3)) /* If documented on, must be required or optional.  2 - Do no display, 3 - Display Only */
/* Order fields template */
, (inner join order_entry_fields oefi
           on oefi.oe_field_id = oeff.oe_field_id)
, (inner join code_value cv
           on cv.code_value = oefi.oe_field_id
          and cv.code_set = 16449)
, (left join code_value_alias cva 
          on cva.code_value = cv.code_value
         and cva.code_set = cv.code_set
          and cva.contributor_source_cd in (select code_value from code_value where display = 'DGEN'))
/* Order detail value */
, (inner join order_detail od
           on od.oe_field_id = oefi.oe_field_id
          and od.order_id = o.order_id)
WHERE o.order_id in ( 3712034153) ;( 3711898657.00, 3711898667,  3711898697) ;(3710696099, 3710696047,  3710709457,   3710709475)
order by o.order_id, oeff.group_seq,oeff.field_seq
with format(date,";;q")
