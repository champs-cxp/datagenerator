/* Get configuration settings of a contributor system */
select domain = curdomain
     , contributor_system = uar_get_code_display(c.act_contributor_system_cd)
     , organization = o.org_name
     , contributor_source = uar_get_code_display(c.contributor_source_cd)
     , contributor_system_type = uar_get_code_display(c.contr_sys_type_cd)
     , message_format = uar_get_code_display(c.message_format_cd)
     , c.active_status_dt_tm
from contributor_system c
, (inner join organization o
           on o.organization_id =  c.organization_id)
, (inner join code_value cv
           on cv.code_value = c.contributor_system_cd
          and cv.display_key =  "DGEN")
where c.active_ind  = 1
with nocounter, separator=" ", format, time = 30
go