/* Codeset aliases for importing into Access DataBase
 * Do NOT change order of presentation - Data Generator imports the csv file, expecting a specific order of the columns/attributes 
 *
 * The big four are :
 * ORU 
 *    72 - event_code (do in Bedrock)
 *    14003 - DTA - run dta_to_alias_for_order.prg
 * ORM
 *    200 - order_catalog - do in bedrock
 *    16449 - order_detail - tODO: run order_detail_alias.prg
 */
select CONTRIBUTOR_SOURCE=cvcs.display
     , cv.code_set
     , CODE_SET_DISP=cvs.display
     , CODE_VALUE_DISP=cv.display
     , cva.alias
     , cv.CODE_VALUE
from code_value_alias cva
   , (inner join code_value cv
             on cv.code_value = cva.code_value
            and cv.active_ind = 1)
   , (inner join code_value cvcs
             on cvcs.code_value = cva.contributor_source_cd
            and cvcs.code_set = 73
            and cvcs.display_key in ('DGEN','EPICADT')) ;replace with display value for contributor source
   , (inner join code_value_set cvs
              on cvs.code_set = cv.code_set)
where cva.code_set != 220 /* exclude location aliases */
order by cvcs.display,cv.code_set, cvs.display,cv.display, cva.alias
go
