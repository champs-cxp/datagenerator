select MRN=pa.alias
     , CSN=ea.alias
     , PtLastName=p.name_last_key
     , PtFirstName=p.name_first_key
     , DOB=p.birth_dt_tm "YYYYMMDD;;D"
     , SEX=cvas.alias
     , AdmissionDate=e.reg_dt_tm "YYYYMMDD;;D"
     , AdmissionTime=e.reg_dt_tm "HHMM;;M"
     , DischargeDate=cnvtdatetime(curdate-1,curtime3) "YYYYMMDD;;D"
     , DischargeTime=cnvtdatetime(curdate-1,curtime3) "HHMM;;M"
     , PatientType=cvaec.alias 
     , Site=cvaf.alias
     , Building=SUBSTRING(findstring("^~",cvabld.alias,1,1)+2,40,cvabld.alias)
     , NurseStation= SUBSTRING(findstring("^~",cvanu.alias,1,1)+2,40,cvanu.alias)
     , Room=SUBSTRING(findstring("^~",cvar.alias,1,1)+2,40,cvar.alias)
     , BED=SUBSTRING(findstring("^~",cvab.alias,1,1)+2,40,cvab.alias)
     , location=uar_get_code_display(e.loc_nurse_unit_cd)
from encounter e
, (inner join encntr_alias ea on ea.encntr_id = e.encntr_id
          and ea.active_ind = 1
          and ea.end_effective_dt_tm > sysdate
          and ea.encntr_alias_type_cd =  1077.00)
;                                        (select code_value
;                                           from code_value
;                                          where code_set = 319
;                                            and cdf_meaning = 'FIN NBR')
, (inner join person p on p.person_id = e.person_id)
, (inner join person_alias pa on pa.person_id = p.person_id
          and pa.active_ind=1
          and pa.end_effective_dt_tm > sysdate
          and pa.person_alias_type_cd = 10.00
;                                        (select code_value
;                                           from code_value
;                                          where code_set = 4
;                                            and cdf_meaning = 'MRN')
          and pa.alias_pool_cd  =  3110551.00)
;                                        (select code_value
;                                           from code_value
;                                          where code_set = 263
;                                            and display_key = 'CHBMRN')
, (inner join code_value_alias cvas on cvas.code_value = p.sex_cd
          and cvas.code_set = 57
          and cvas.contributor_source_cd = 3110566.00)
;                                             (select code_value
;                                           from code_value
;                                          where code_set = 89
;                                            and display_key = 'EPICADT')
, (inner join code_value_alias cvaec on cvaec.code_value = e.encntr_type_cd
          and cvaec.code_set = 71
          and cvaec.contributor_source_cd = 3110566.00)
, (inner join code_value_alias cvaf on cvaf.code_value = e.loc_facility_cd
          and cvaf.code_set = 220
          and cvaf.contributor_source_cd = 3110566.00)
, (inner join code_value_alias cvabld on cvabld.code_value = e.loc_building_cd
          and cvabld.code_set = 220
          and cvabld.contributor_source_cd = 3110566.00)
, (inner join code_value_alias cvanu on cvanu.code_value = e.loc_nurse_unit_cd
          and cvanu.code_set = 220
          and cvanu.contributor_source_cd = 3110566.00)
, (inner join code_value_alias cvar on cvar.code_value = e.loc_room_cd
          and cvar.code_set = 220
          and cvar.contributor_source_cd = 3110566.00)
, (left join code_value_alias cvab on cvab.code_value = e.loc_bed_cd
          and cvab.code_set = 220
          and cvab.contributor_source_cd = 3110566.00) ; EPICADT
where e.encntr_status_cd = 854.00
;                         (select code_value from code_value where code_set = 261 and cdf_meaning = 'ACTIVE')
  and e.encntr_type_cd in (309308.00, 309312.00,309310.00)
;                         (select code_value from code_value where code_set = 71 and display_key = 'OBSERVATION')
  and e.loc_facility_cd = 3110560.00
;                          (select code_value
;                             from code_value
;                            where code_set = 220
;                              and cdf_meaning = 'FACILITY'
;                              and display_key = 'BOSTONCHILDRENSHOSPITAL')
;   and e.loc_nurse_unit_cd =   3135811.00; 3995159.00; 3995219.00; 3135811.00
;                          (select code_value
;                             from code_value
;                            where code_set = 220
;                              and cdf_meaning = 'NURSEUNIT'
;                              and display_key = '10NORTHWEST')
;  and datetimediff(sysdate,e.reg_dt_tm) > 60
   and e.reg_dt_tm > cnvtdatetime("20-Jun-2021")  /* EPIC TEST Refresh */
;  and ea.alias = '6128127875*'; '8*'
;and e.loc_bed_cd =   390684445.00
; and e.encntr_id = 94313714
 and ea.alias = '6*'
order by location,csn,ptlastname
;with maxrec=10
go
; 
select *
from code_value
where code_set = 220
and display_key = '10NORTH*'
go

select code_value
from code_value
where code_set = 73
and display_key = 'EPICADT'
go
                                            
select code_value from code_value where code_set = 71 and display_key = 'EMERGENCY' go
select * from encounter e where e.encntr_id =     94420053.00;   94407618.00;  94408136.00 go 
select * from encntr_alias ea where ea.alias  = '8100023516' go
select * from person_alias pa where pa.alias  = '4652314' go ;   18284647.00

; encntr_id in discharged status, but no disch_dt_tm: (94407618.00, 94408136.00)
; encntr_id in cancelled status, but no disch_dt_tm   94420053.00 (scheduled 8/10/21, but created 4/22/21)


select *
from encntr_loc_hist 
where encntr_id =  94420053.00
go
