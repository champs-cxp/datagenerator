/* updt_task is distinctively set to 4170142 for prsnl added by Content Mnager */
select  ExternalID=pra.alias
     , pr.name_last
     , pr.name_first
     , POSITION=cvpos.display
     , pr.physician_ind
     , pr.username
     , contributor_source=uar_get_code_display(pra.contributor_system_cd)
from prsnl pr
, (inner join prsnl_alias pra
           on pra.person_id = pr.person_id
          and pra.active_ind = 1
          and pra.end_effective_dt_tm > sysdate
          and pra.alias_pool_cd in (select code_value
                                      from code_value
                                     where code_set = 263
                                       and active_ind = 1
                                       and display_key = 'CHBEPDS')
          and pra.prsnl_alias_type_cd in (select code_value
                                            from code_value
                                           where code_set = 320
                                            and active_ind = 1
                                            and cdf_meaning = 'EXTERNALID')
           and pra.contributor_system_cd = 0)
, (inner join code_value cvpos
           on cvpos.code_value = pr.position_cd
          and cvpos.code_set = 88
          and cvpos.active_ind = 1)        
where pr.updt_dt_tm <  sysdate
and pr.updt_task = 4170142 ; Content Manager
go

/* AuthView touches ea_user only */
select *
from ea_user eau
where eau.username in ('DGMD1','DGMD2','DGMD3','DGMD4')
with format(date,";;q")
go
